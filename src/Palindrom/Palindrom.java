package Palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String s = scan.nextLine();
        if (isPalindrome(s)) {
            System.out.println("Это Палиндром!");
        } else {
            System.out.println("Это не Палиндром!");
        }
    }

    private static boolean isPalindrome(String text) {
        text = text.replaceAll("([-\\s\"{},.;'`@#$%_^&*()№:!/?])","");
        StringBuilder strBuilder = new StringBuilder(text);
        strBuilder.reverse();
        String invertedText = strBuilder.toString();

        return text.equalsIgnoreCase(invertedText);
    }
}